-- Artist that has letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- songs that has a length <230
SELECT * FROM songs WHERE length < 230;

-- ALBUMS AND SONGS (album name, song name song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- ARTISTS AND ALBUMS (all albums that has letter a in its name)
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- SORT ALBUMS IN Z-A (SHOW ONLY FIRST 4)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- JOIN ALBUMS AND SONGS (SORT ALBUMS FROM Z-A)
SELECT * FROM albums 
    JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC;